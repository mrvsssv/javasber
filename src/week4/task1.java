package week4;

import java.util.Scanner;

public class task1 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int n = console.nextInt();

        int res = 1;
        for (int i = 2; i <= n; i++) {
            res *= i;
        }

        System.out.println(res);
    }
}
