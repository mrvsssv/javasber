package week4;

import java.util.Scanner;

public class task4 {
    public static void main(String[] args) {
//        Case1:
//        Scanner scanner = new Scanner(System.in);
//
//        double percent = (scanner.nextInt()) / 100.00;
//
//        int i = 0;
//
//        double sum = 1000;
//        for (; sum <= 1100; sum += sum * percent) {
//            i++;
//        }
//
//        System.out.println("Количество месяцев: " + i);
//        System.out.println("Сумма на вкладе: " + sum);

//        Case2:
        Scanner scanner = new Scanner(System.in);
        int p = scanner.nextInt();

        double start = 1000;
        double limit = 1100;

        int month = 0;

        while (start < limit) {
            start += (start * p) / 100.;
            month++;
        }
        System.out.println(month);
        System.out.println(start);
    }

}
