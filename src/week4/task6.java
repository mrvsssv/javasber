package week4;

/*
Дано целое число n.
Найти n число Фибоначчи с помощью цикла.
//0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946
F0 = 0
F1 = 1
Fn = Fn-1 + Fn-2
Фибоначчи - последовательность, в которой первые два числа равны 0 и 1,
а каждое последующее число равно сумме двух предыдущих чисел
Fn = Fn-1 + Fn-2, n>=2
 */

import java.util.Scanner;

public class task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int m0 = 0;
        System.out.print(m0 + " ");
        int m1 = 1;
        System.out.print(m1 + " ");
        int m2;
        for (int i = 2; i <= n; i++) {
            m2 = m0 + m1;
            System.out.print(m2 + " ");
            m0 = m1;
            m1 = m2;
        }
        System.out.println();
    }
}
