package week4;

import java.util.Scanner;

public class task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int sum = 0;
        while (n > 0) {
            sum += n % 10;
            System.out.println("Sum in While Loop: " + sum);
            n /= 10;
        }
        System.out.println(sum);

        int total = 0;
        for (int m = scanner.nextInt(); m > 0; m /= 10) {
            total += m % 10;
        }
        System.out.println(total);

    }
}
