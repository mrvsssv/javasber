package week9.part1.arraylistexample;

import java.util.ArrayList;

public class BaseOperation {
    public static void main(String[] args) {
        // Создание и заполнения ArrayList
        ArrayList<Car> cars = new ArrayList<>();
        cars.add(new Car("BMW"));
        cars.add(new Car("VOLVO"));
        cars.add(new Car("TOYOTA"));

        // Итерирования (перебор нашего ArrayList)
        System.out.println("Машина в нашем списке: ");
        for (Car car: cars) {
            System.out.println(car.getCarBrand());
        }
        System.out.println();

        // Итерирования c помощью лямба выражения
        System.out.println("Машина в нашем списке: ");
        cars.forEach(car -> {
            System.out.println(car.getCarBrand());
            // Какие-то еще действия
        });
        System.out.println();

        // Удаления элемента из ArrayList
        cars.remove(cars.size() - 1);

        // Итерирования c помощью лямба выражения
        cars.forEach(car -> System.out.println(car.getCarBrand()));
        System.out.println();

        // ArrayList.contains()
        System.out.println(cars.contains("BMW"));
        System.out.println(cars.contains(new Car("BMW")));

        Car bmw = cars.get(0);

        System.out.println(cars.contains(bmw));
    }
}
