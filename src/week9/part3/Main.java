package week9.part3;

public class Main {
    public static void main(String[] args) {
        IceCream cherry = IceCreamFactory.getIceCream(IceCreamType.CHERRY);
        IceCream chocolate = IceCreamFactory.getIceCream(IceCreamType.CHOCOLATE);
        IceCream vanila = IceCreamFactory.getIceCream(IceCreamType.VANILLA);

        cherry.printIngredients();
        chocolate.printIngredients();
        vanila.printIngredients();
    }
}
