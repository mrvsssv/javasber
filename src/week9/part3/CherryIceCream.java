package week9.part3;

public class CherryIceCream implements IceCream {
    @Override
    public void printIngredients() {
        System.out.println("Вишня, сливки, крем");
    }
}
