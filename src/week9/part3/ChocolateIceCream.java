package week9.part3;

public class ChocolateIceCream implements IceCream {

    @Override
    public void printIngredients() {
        System.out.println("Шоколад, сливки, крем");
    }
}
