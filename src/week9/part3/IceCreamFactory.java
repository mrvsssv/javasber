package week9.part3;

public class IceCreamFactory {
    public static IceCream getIceCream(IceCreamType iceCreamType) {
        return switch (iceCreamType) {
            case CHERRY -> new CherryIceCream();
            case VANILLA -> new VanillaIceCream();
            case CHOCOLATE -> new ChocolateIceCream();
        };
    }

    private IceCreamFactory(){}
}
