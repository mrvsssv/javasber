package week9.part3;

public enum IceCreamType {
    CHERRY,
    CHOCOLATE,
    VANILLA
}
