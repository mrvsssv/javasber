package week9.part2.logger;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class FileLoggerCsv extends FileOutputter implements Logger {
    public FileLoggerCsv(String fileName) {
        this.fileName = fileName + getFileExtension();
    }

    public FileLoggerCsv() {
        this.fileName = getDefaultFileName() + getFileExtension();
    }


    @Override
    public String getFileExtension() {
        return ".csv";
    }

    public void log(String message) {
        try (Writer writer = new FileWriter(fileName, true)) {
            writer.write(message + "\n");
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }
}
