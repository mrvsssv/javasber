package week9.part2.logger;

public abstract class FileOutputter {
    protected String fileName = "defaultFileName";

    protected FileOutputter(String defaultFileName) {
        this.fileName = defaultFileName;
    }

    protected FileOutputter() {
    }

    protected String getDefaultFileName() {
        return fileName;
    }

    protected String getFileExtension() {
        return ".txt";
    }

}
