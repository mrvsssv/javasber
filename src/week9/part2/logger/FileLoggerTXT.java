package week9.part2.logger;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class FileLoggerTXT
        extends FileOutputter
        implements Logger {

    public FileLoggerTXT(String fileName) {
        this.fileName = fileName + getFileExtension();
    }

    public FileLoggerTXT() {
        this.fileName = getDefaultFileName() + getFileExtension();
    }

    @Override
    public String getFileExtension() {
        return ".txt";
    }

    public void log(String message) {
        try (Writer writer = new FileWriter(fileName, true)) {
            writer.write(message + "\n");
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }
}
