package week9.part2.logger;

public class Main {
    public static void main(String[] args) {
        // Part1
        Logger logger = new ConsoleLogger();

        Bulb bulb = new Bulb(logger);
        bulb.turnOn();
        bulb.turnOff();
        bulb.isShining();

        // Part2

        Logger logger2 = new FileLoggerTXT();

        Bulb bulb2 = new Bulb(logger2);
        bulb2.turnOn();
        bulb2.turnOff();
        bulb2.isShining();

        Logger logger3 = new FileLoggerCsv("test_FileLogger_CSV");

        Bulb bulb3 = new Bulb(logger3);
        bulb3.turnOn();
        bulb3.turnOff();
        bulb3.isShining();
    }
}
