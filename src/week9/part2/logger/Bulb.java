package week9.part2.logger;

public class Bulb {
    private boolean toggle;
    private Logger logger;

    public Bulb(Logger logger) {
        this.logger = logger;
    }

    public  void turnOn() {
        this.toggle = true;
        logger.log("Bulb is On!");
    }

    public  void turnOff() {
        this.toggle = false;
        logger.log("Bulb is Off!");
    }

    public boolean isShining() {
        logger.log("Getting information about Bulb " + toggle);
        return this.toggle;
    }
}

