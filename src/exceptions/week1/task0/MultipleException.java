package exceptions.week1.task0;

public class MultipleException {
    public static void main(String[] args) {
        try {
            someMethodThrowMyMathException();
            toDivideMyMathException(100,0);
            simpleThrowRuntimeException();
        } catch (MyMathException e) {
            System.err.println("LOG: Деление на 0.");
        } catch (ArrayIndexOutOfBoundsException e2) {
            System.err.println("LOG: Проблема с массивом");
        } catch (RuntimeException e3) {
            System.err.println("LOG: Runtime.");
        }

    }

    public static void toDivideMyMathException(int a, int b) throws MyMathException {
        try {
            System.out.println(a / b);
        } catch (ArithmeticException e) {
            throw new MyMathException(e.getMessage());
        }
    }

    public static void someMethodThrowMyMathException() {
        int[] arr = new int[10];
        System.out.println(arr[10]);
    }

    public static void simpleThrowRuntimeException() {
        throw new RuntimeException();
    }
}
