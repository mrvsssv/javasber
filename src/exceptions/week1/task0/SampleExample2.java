package exceptions.week1.task0;

public class SampleExample2 {
    public static void main(String[] args) throws MyMathException {
        int result = division(55, 0);
        System.out.println(result);
    }

    public static int division(int a, int b) throws MyMathException {
        try {
            return a / b;
        } catch (ArithmeticException e) {
            System.err.println("ArithmeticException -> FOUND!");
            throw new MyMathException("Произошло деление на 0", e);
        } finally {
            System.out.println("Hello From Finally");
        }
    }
}
