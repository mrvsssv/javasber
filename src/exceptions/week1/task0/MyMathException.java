package exceptions.week1.task0;

public class MyMathException
        extends Exception {
    public MyMathException(){}
    public MyMathException(String message) {
        super(message);
        // дополнительные действия
    }

    public MyMathException(String message, Throwable throwable) {
        super(message, throwable);
        // дополнительные действия
    }

}
