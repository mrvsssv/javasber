package exceptions.week1.task0;

public class SampleExample {
    public static void main(String[] args) {
        try {
            int result = division(55, 0);
            System.out.println(result);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static int division(int a, int b) throws IllegalAccessException {
        if (b == 0) {
            throw new IllegalAccessException("The divisor cannot be zero!");
        }
        return a / b;
    }
}
