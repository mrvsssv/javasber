package exceptions.week1.task4.exceptions;

public class MyDivisionByZeroException extends MyBaseException {
    public MyDivisionByZeroException(String errorMessage) {
        super(errorMessage);
    }

    public MyDivisionByZeroException() {
        super("Деление на 0");
    }
}
