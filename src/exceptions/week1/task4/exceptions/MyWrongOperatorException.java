package exceptions.week1.task4.exceptions;

public class MyWrongOperatorException extends MyBaseException {
    public MyWrongOperatorException(String errorMessage) {
        super(errorMessage);
    }

    public MyWrongOperatorException() {
        super("Не допустимый оператор для калькулятора");
    }
}
