package exceptions.week1.task4;

public class CalcTest {
    public static void main(String[] args) {
        Calc calc = new Calc();

        try {
            calc.input();
            System.out.println(calc.calculate());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
