package exceptions.week1.task3;

/*
Считать построчно из input.txt и каждую строку записывать в output.txt
без промежуточного буфера.
 */

import java.io.*;
import java.util.Scanner;

public class FileReadWrite {
    private static final String PKG_DIRECTORY = "/Users/vssv/Downloads/JavaCoursesBasic1/src/exceptions/week1/task3";
    private static final String OUTPUT_FILE_NAME = "output.txt";
    private static final String INPUT_FILE_NAME = "input.txt";

    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(new File(PKG_DIRECTORY + "/" + INPUT_FILE_NAME));
             Writer wr = new FileWriter(PKG_DIRECTORY + "/" + OUTPUT_FILE_NAME)) {
            while (scanner.hasNext()) {
                wr.write(scanner.nextLine() + "\n");
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}