package exceptions.week2.task1;

/*
Создать класс Pair, который умеет хранить два значения:
1) Любого типа (T, V)
2) Одинакового типа
3) строка и число только
 */
public class Main {
    public static void main(String[] args) {
        Pair<Integer,String> pair = new Pair<>(10,"Apple");
        pair.setKey(1);
        pair.setValue("Значение");
        pair.print();

    }
}