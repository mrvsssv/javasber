package exceptions.week2.task1;

public class Pair<T extends Number, U extends String> {
    private T key;
    private U value;

    public Pair(T key, U value) {
        this.key = key;
        this.value = value;
    }

    public T getKey() {
        return key;
    }

    public U getValue() {
        return value;
    }

    public void setKey(T key) {
        this.key = key;
    }

    public void setValue(U value) {
        this.value = value;
    }

    public static <K extends Number,U extends String> boolean compare(Pair<K,U> p1, Pair<K,U> p2) {
        return p1.getKey().equals(p2.getKey()) && p1.getValue().equals(p2.getValue());
    }

    public void print() {
        System.out.println("Key: " + key + " Value: " + value);
    }
}
