package exceptions.week2.task5;

import java.util.HashSet;
import java.util.TreeSet;

/*
Создать метод, переводящий из HashSet в TreeSet. Вывести оба варианта.
 */
public class ConvertHashSet {
    private ConvertHashSet() {}

    public static <T>TreeSet<T> convertTree(HashSet<T> hashSet) {
        return new TreeSet<>(hashSet);
    }

    public static <T>HashSet<T> convertTree(TreeSet<T> treeSet) {
        return new HashSet<>(treeSet);
    }
}
