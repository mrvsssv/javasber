package exceptions.week2.task10;

import java.util.List;

public class MainPecs {
    static class Item {

    }

    static class Book extends Item {
        String name;
    }

    static class Phone extends Item {
        String model;
    }

    //PECS
    // Producer Extents, Consumer Super

    public static void copy(List<? extends  Item> src, List<? super  Item> dst) {
        for(Item item: src) {
            dst.add(item);
        }
    }
}
