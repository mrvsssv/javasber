package exceptions.week2.task2;

import exceptions.week2.task1.Pair;

/*
    Написать для Pair геттеры/сеттеры
 */
public class Main {
    public static void main(String[] args) {
        Pair<Double,String> p1 = new Pair<>(1d, "orange");
        Pair<Double,String> p3 = new Pair<>(1d, "orange");
        Pair<Double,String> p2 = new Pair<>(1d, "apple");

        System.out.println(Pair.compare(p1,p2));
        System.out.println(Pair.compare(p1,p3));
    }
}
