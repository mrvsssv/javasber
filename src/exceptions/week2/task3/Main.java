package exceptions.week2.task3;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/*
 На вход подаются два сета, вывести уникальные элементы,
 которые встречаются и в первом и во втором.
 */
public class Main {
    public static void main(String[] args) {

        Set<Integer> set1 = new HashSet<>();
        set1.add(0);
        set1.add(1);
        set1.add(2);

        Set<Integer> set2 = new HashSet<>();
        set2.add(0);
        set2.add(3);
        set2.add(2);

        set1.retainAll(set2);

        System.out.println(set1);

        set2.forEach(System.out::println);

        System.out.println(Collections.disjoint(set1,set2));

    }
}

