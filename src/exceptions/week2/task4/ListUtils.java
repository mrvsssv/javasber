package exceptions.week2.task4;

import java.util.List;

public class ListUtils {

    private ListUtils() {
    }

    public static <T> int countIf(List<T> elements, T element) {
        int count = 0;
        for (T elem : elements) {
//            if (elem.equals(element)) {
//                ++count;
//            }
            if (elem == element) {
                ++count;
            }
        }
        return count;
    }
}
