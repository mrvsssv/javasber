package exceptions.week2.task4;

import java.util.ArrayList;
import java.util.List;

/*
Реализовать метод, который считает количество элементов в переданном List
 */
public class Main {
    public static void main(String[] args) {
        List<Boolean> list1 = new ArrayList<>();
        list1.add(true);
        list1.add(true);
        list1.add(false);
        list1.add(true);

        System.out.println(ListUtils.countIf(list1,true));
        System.out.println(ListUtils.countIf(list1,false));

        List<String> list2 = new ArrayList<>();
        list2.add("abc");
        list2.add("abc");
        list2.add("ddd");
        list2.add("abc");

        System.out.println(ListUtils.countIf(list2,"abc"));
        System.out.println(ListUtils.countIf(list2,"ddd"));
    }
}
