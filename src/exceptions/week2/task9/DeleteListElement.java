package exceptions.week2.task9;

import java.util.ArrayList;
import java.util.Iterator;

/*
Удалить элемент из списка, если он нечетный.
 */
public class DeleteListElement {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);

//        for (Integer elem: list) {
//            if (elem % 2 != 0) {
//                list.remove(elem);
//            }
//        }

        // 1 способ
//        list.removeIf(elem -> elem % 2 != 0);

        // 2 способ - создать компию списка и работать с ней
        // 3 способ - iterator

        Iterator<Integer> iterator = list.iterator();

        while (iterator.hasNext()) {
            int elem = iterator.next();
            if (elem % 2 != 0)
                iterator.remove();
        }

        // 4 способ через fori


        System.out.println(list);
    }
}
