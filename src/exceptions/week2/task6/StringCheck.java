package exceptions.week2.task6;

import java.util.*;

/*
[С собеседований]
На вход подается строка, состоящая из маленьких латинских символов.
Проверить, что в строке встречаются все символы английского алфавита хотя бы раз:
thequickbrownfoxjumpsoverthelazydog -> true
sdfaaaa -> false
 */
public class StringCheck {
    public static void main(String[] args) {
        System.out.println(CheckingStringForEnglishAlphabet("thequickbrownfoxjumpsoverthelazydog"));
        System.out.println(CheckingStringForEnglishAlphabet("sdfaaaa"));

        String str = "thequickbrownfoxjumpsoverthelazydog";
        long s = str.chars().distinct().count();
        System.out.println(s == 26);

        String str2 = "sdfaaaa";
    }

    public static boolean CheckingStringForEnglishAlphabet(String str) {
        if (Objects.isNull(str) || str.length() < 26) {
            return false;
        }
        Set<Character> set = new HashSet<>();
        for (char ch : str.toCharArray()) {
            set.add(ch);
        }
        return set.size() == 26;
    }
}

