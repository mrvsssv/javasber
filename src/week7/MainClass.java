package week7;

import week7.task2.TempType;
import week7.task2.Thermometer;
import week7.task3.Robot;
import week7.task4.FieldValidator;
import week7.task5.SimpleStack;
import week7.task7.Account;

public class MainClass {
    public static void main(String[] args) {
        // Task 1
//        Bulb lamp = new Bulb();
//        lamp.turnOn();
//        System.out.println(lamp.isShining());

        // Task 2
//        Thermometer term = new Thermometer(18.0, TempType.Celsius);
//        System.out.println(term.getTempCelsius());
//        System.out.println(term.getTempFahrenheit());
//        Thermometer term2 = new Thermometer(130.0, TempType.Fahrenheit);
//        System.out.println(term2.getTempCelsius());
//        System.out.println(term2.getTempFahrenheit());

        // Task 3
//        Robot robot = new Robot();
//        robot.go();
//        robot.go();
//        robot.go();
//        robot.printCoordinates();
//        robot.turnLeft();
//        robot.go();
//        robot.go();
//        robot.turnLeft();
//        robot.go();
//        robot.go();
//        robot.printCoordinates();

        // Task 4
//        System.out.println(FieldValidator.validateName("Denis"));
//        System.out.println(FieldValidator.validatePhone("+79177067860"));
//        System.out.println(FieldValidator.validateDate("13.10.2000"));
//        System.out.println(FieldValidator.validateEmail("mrvsssv@gmail.com"));

        // Task 5
//        SimpleStack stack = new SimpleStack(3);
//        System.out.println(stack.isEmpty());
//        stack.push(1);
//        stack.push(2);
//        stack.push(3);
//        stack.pop();
//        System.out.println(stack.top());
//        System.out.println(stack.isFull());

        // Task 7

        Account.Builder accountBuilder = Account.newBuilder();
        accountBuilder.setToken("TOKEN");
        accountBuilder.setUserId("123");

        Account account = accountBuilder.build();

        Account account2 = Account.newBuilder()
                .setToken("asd")
                .setUserId("345")
                .build();
    }
}
