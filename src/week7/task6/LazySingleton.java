package week7.task6;

/*
Реализовать простой Design Pattern Singleton двух типов:

- Lazy loading (ленивая/отложенная загрузка) — создание экземпляра по требованию.
- Eager loading (нетерпеливая/ранняя загрузка) — ранняя загрузка, создание экземпляра при инициализации.

 */
public class LazySingleton {
    private static LazySingleton INSTANCE;

    private LazySingleton(){}

    public static LazySingleton getINSTANCE() {
        if (INSTANCE == null) {
            INSTANCE = new LazySingleton();
        }
        return INSTANCE;
    }


}
