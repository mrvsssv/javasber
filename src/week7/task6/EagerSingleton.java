package week7.task6;

public class EagerSingleton {
    private static final EagerSingleton INSTANCE = new EagerSingleton();
    private EagerSingleton(){}

    public static EagerSingleton getINSTANCE() {
        return INSTANCE;
    }
}
