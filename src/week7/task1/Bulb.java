package week7.task1;

/*
Реализовать класс “Лампа”.
Методы:
- включить лампу
- выключить лампу
- получить текущее состояние
 */

public class Bulb {
    private boolean toggle;

    public  void turnOn() {
        this.toggle = true;
    }

    public  void turnOff() {
        this.toggle = false;
    }

    public boolean isShining() {
        return this.toggle;
    }
}
