package week5;

import java.util.Arrays;
import java.util.Scanner;

/*
   На вход подается два отсортированных массива.
   Нужно создать отсортированный третий массив,
   состоящий из элементов первых двух.

   Входные данные:
   5
   1 2 3 4 7

   2
   1 6

   Выходные данные:
   1 1 2 3 4 6 7
    */
public class Task4 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        int[] arr1 = addArray(scanner.nextInt());
        int[] arr2 = addArray(scanner.nextInt());

//        mergeTwoArraysWithLoop(arr1, arr2);
//        mergeTwoArraysWithSystemArrayCopy(arr1, arr2);
        mergeTwoArrays(arr1, arr2);
    }

    public static void mergeTwoArrays(int[] arr1, int[] arr2) {
        int[] mergeArray = new int[arr1.length + arr2.length];
        int i = 0, j = 0, k = 0;
        while (i < arr1.length && j < arr2.length) {
            if (arr1[i] < arr2[j]) {
                mergeArray[k++] = arr1[i++];
            } else {
                mergeArray[k++] = arr2[j++];
            }
        }

        while (i < arr1.length) {
            mergeArray[k++] = arr1[i++];
        }

        while (j < arr2.length) {
            mergeArray[k++] = arr2[j++];
        }

        System.out.println(Arrays.toString(mergeArray));
    }

    public static void mergeTwoArraysWithSystemArrayCopy(int[] arr1, int[] arr2) {
        int[] mergeArray = new int[arr1.length + arr2.length];

        System.arraycopy(arr1, 0, mergeArray, 0, arr1.length);
        System.arraycopy(arr2, 0, mergeArray, arr1.length, arr2.length);

        Arrays.sort(mergeArray);
        System.out.println(Arrays.toString(mergeArray));
    }

    /**
     * Метод делает слияние двух отсортированных массивов в третий результирующий
     *
     * @param arr1 - первый массив
     * @param arr2 - второй массив
     */
    public static void mergeTwoArraysWithLoop(int[] arr1, int[] arr2) {
        int[] mergeArray = new int[arr1.length + arr2.length];
        int pos = 0;

        for (int element : arr1) {
            mergeArray[pos] = element;
            pos++;
        }

        for (int element : arr2) {
            mergeArray[pos] = element;
            pos++;
        }
        Arrays.sort(mergeArray);
        System.out.println(Arrays.toString(mergeArray));
    }

    public static int[] addArray(int arrayLenght) {
        int[] arr = new int[arrayLenght];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = scanner.nextInt();
        }
        return arr;
    }

}
