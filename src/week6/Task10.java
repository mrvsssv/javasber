package week6;

import java.util.Scanner;

/*
Реализовать алгоритм Евклида рекурсивно.
Алгоритм для нахождения наибольшего общего делителя двух целых чисел

Входные данные
15 25
Выходные данные
5

Входные данные
96 144
Выходные данные
48
*/
public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        int m = scanner.nextInt();

        System.out.println(euclidean(x, m));
    }

    private static int euclidean(int m, int n) {
        if (m != n) {
            if (m > n) {
                m = m - n;
            } else {
                n = n - m;
            }
            return euclidean(m, n);
        } else {
            return m;
        }
    }
}
