package week6;

import java.util.Scanner;

/*
На вход подается натуральное число N.
Необходимо проверить, является ли оно степенью двойки (решить через рекурсию).
Вывести true, если является и false иначе.

Входные данные
4
Выходные данные
true

Входные данные
5
Выходные данные
false
 */
public class Task9 {
    public static void main(String[] args) {
        int n = new Scanner(System.in).nextInt();
        System.out.println(isRecourse(n, 2));
    }

    private static boolean isRecourse(int n, int y) {
        if (n == y) {
            return true;
        }
        if (y > n) {
            return false;
        }
        return isRecourse(n, y * 2);
    }
}