package week6;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        String input = new Scanner(System.in).nextLine();
        System.out.println(reverseString(input));
    }

    private static String reverseString(String input) {
        if (input.isEmpty()) {
            return input;
        } else {
            return reverseString(input.substring(1)) + input.charAt(0);
        }
    }
}
