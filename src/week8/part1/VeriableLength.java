package week8.part1;

public class VeriableLength {

    static int sum(int... numbers) {
        int sum = 0;
        for (int number : numbers) {
            sum += number;
        }
        return sum;
    }

    static boolean findChar(Character ch, String... strings) {
        for (String string : strings) {
            if (string.indexOf(ch) != -1) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {


        System.out.println(sum(1, 3, 5, 6, 7, 8, 9));
        System.out.println(sum(1, 2));

        System.out.println(findChar('a', "python", "java"));
        System.out.println(findChar('q', "python", "java"));

        System.out.println(String.format("This is an integer: %d", 123));
    }
}
