package week8.part1;

public enum WeekDays {
    MONDAY(1, "Понедельник"),
    TUESDAY(2, "Вторник"),
    WEDNESDAY(3, "Среда"),
    THURSDAY(4, "Четверг"),
    FRIDAY(5, "Пятница"),
    SATURDAY(6, "Суббота"),
    SUNDAY(7, "Воскресенье"),
    NOT_A_DAY(-1, "Такого дня недели не существует");

    private final int dayNumber;
    private final String name;

    WeekDays(int dayNumber, String name) {
        this.dayNumber = dayNumber;
        this.name = name;
    }

    public static WeekDays ofNumber(int dayNumber) {
        for (WeekDays ownershipType : values()) {
            if (ownershipType.dayNumber == dayNumber) {
                return ownershipType;
            }
        }
        throw new IllegalArgumentException("Неизвестное значение номера для дня недели: " + dayNumber);
    }

    public int getDayNumber() {
        return dayNumber;
    }

    public String getName() {
        return name;
    }

    public static WeekDays ofName(String name) {
        for (WeekDays ownershipType : values()) {
            if (ownershipType.name.equals(name)) {
                return ownershipType;
            }
        }
        throw new IllegalArgumentException("Неизвестное значение номера для дня недели: " + name);
    }

    @Override
    public String toString() {
        return "WeekDays{" +
                "dayNumber=" + dayNumber +
                ", name='" + name + '\'' +
                '}';
    }
}
