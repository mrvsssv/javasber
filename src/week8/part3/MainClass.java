package week8.part3;

public class MainClass {
    public static void main(String[] args) {
//        SimpleArrayList my = new SimpleArrayList();
//        my.add(12);
//        my.add(99);
//        System.out.println("List size: " + my.size());
//        System.out.println("Element with first index: " + my.get(1));

        SimpleLinkedList link = new SimpleLinkedList();
        link.addAtHead(10);
        link.addAtHead(50);
        link.addAtTail(99);
//        System.out.println("List size: " + link.size());
        System.out.println("Element with first index: " + link.get(1));
    }
}
