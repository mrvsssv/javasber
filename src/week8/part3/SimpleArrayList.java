package week8.part3;

import java.util.Arrays;

/*
Примитивная реализация ArrayList.
Массив только int, из методов только добавлять элемент,
получать size и увеличивать капасити, когда добавляется новый.
 */
public class SimpleArrayList {
    private int size;
    private int capacity;
    private int currentIndex;
    private int[] arr;
    private static final int DEFAULT_CAPACITY = 5;

    public SimpleArrayList() {
        this(DEFAULT_CAPACITY);
    }

    public SimpleArrayList(int size) {
        this.size = 0;
        this.arr = new int[size];
        this.capacity = DEFAULT_CAPACITY;
        this.currentIndex = 0;
    }

    public void add(int element) {
        if (currentIndex >= capacity) {
            capacity *= 2;
            arr = Arrays.copyOf(arr, capacity);
        }
        arr[currentIndex++] = element;
        size++;
    }

    public int get(int index) {
        if (index < 0 || index > size) {
            System.out.println("Impossible to get element for this index: " + index);
            return -1;
        }
        return arr[index];
    }

    public int size() {
        return size;
    }

}
