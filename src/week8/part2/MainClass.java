package week8.part2;

import week8.part1.WeekDays;

public class MainClass {
    public static void main(String[] args) {
        // Task1
//        System.out.println(WeekDays.MONDAY);
//        WeekDays dayOfWeek = WeekDays.ofNumber(7);

        String name = "Среда";
        System.out.println("Порядковый номер дня недели: " + name + " = " + WeekDays.ofName(name).getDayNumber());
        int number = 7;
        System.out.println("День недели по номеру: " + number + " = " + WeekDays.ofNumber(number).getName());
    }
}
