package week8.part2;

/*
Читаем данные из файла
В файле у нас будут дни недели текстом
Результат пишем в output.txt файл
 */

import week8.part1.WeekDays;

import java.io.*;
import java.util.Scanner;

public class ReadFile {

    private static final String PKG_DIRECTORY = "/Users/vssv/Downloads/JavaCoursesBasic1/src/week8/part2";
    private static final String OUTPUT_FILE_NAME = "output.txt";
    private ReadFile(){}

    public static void readDataFromFile(String filePath) throws IOException {
        Scanner scanner = new Scanner(new File(filePath));
        String[] days = new String[10];
        int i = 0;
        while (scanner.hasNextLine()) {
            days[i++] = scanner.nextLine();
            System.out.println(days[i - 1]);
        }

        Writer wr = new FileWriter(PKG_DIRECTORY + "/" + OUTPUT_FILE_NAME);
        for (int j = 0; j < i; j++) {
            String res = "Порядковый номер для дня недели: " + days[j] + " равен: "
                    + WeekDays.ofName(days[j]).getDayNumber() + "\n";
            wr.write(res);
        }
        wr.close();
    }
}
