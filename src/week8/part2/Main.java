package week8.part2;

import java.io.FileNotFoundException;
import java.io.IOException;

import static week8.part2.ReadFile.readDataFromFile;

public class Main {
    public static void main(String[] args) throws IOException {
        String fileLocation = "/Users/vssv/Downloads/JavaCoursesBasic1/src/week8/part2/input.txt";
        readDataFromFile(fileLocation);
    }
}
