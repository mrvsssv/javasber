package week3;


/*
Перевод CamelCase в snake_case
На вход подается строка, состоящая из заглавных и прописных латинских
букв (вида CamelCase). Вывести эту же строку, но состоящую только
из прописных букв (вид snake_case), а перед местом, где была заглавная буква -
должен быть выведен символ нижнего подчеркивания.

ItIsCamelCaseString
it_is_camel_case_string

SomeLongVariable
some_long_variable
 */

import java.util.Scanner;

public class task4 {
    public static void main(String[] args) {
        String inputStr = new Scanner(System.in).nextLine();

        String regex = "(?<=[0-9])(?=[A-Za-z])";
        System.out.println("Enter input text: ");
//        String result2 = inputStr.replaceAll(regex, " ");
//        System.out.println(result2);
        String result = inputStr.replaceAll( "(\\d)([A-Za-z])", "$1 $2" );
        System.out.println(result);

        System.out.println(inputStr.replaceAll("([a-z])([A-Z])", "$1_$2").toLowerCase());
    }
}

